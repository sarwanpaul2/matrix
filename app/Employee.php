<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['name','salary','age','profile_image','address'];
    protected $table = 'employee';
}
