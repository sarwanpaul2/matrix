<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\UserProfile;  
use Validator;
use File;
use URL;



class UserController extends Controller
{
    public function register(Request $request) { 
               
        $validator = Validator::make($request->json()->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'age' => 'required',
            'dob' => 'required',
            'address' => 'required',
            'password' => 'required',
            'profile_picture' => 'required',
            'role' => 'required',            
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->json()->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = UserProfile::create($input); 
        $success['status'] =  200; 
        $success['message'] =  "Successfully Added";
        return response()->json(['success'=>$success]); 
    }

    public function uploadImage(Request $request){
        
        $request->validate([ 
            'profile_picture' =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if ($request->has('profile_picture')) {
            $image = $request->file('profile_picture');
            
            $imageName = time().'.'.$request->profile_picture->extension();  
            $request->profile_picture->move(public_path('images'), $imageName);
            $imageUrl = URL::to('/').'/images/'.$imageName;
            $data['status'] =  200; 
            $data['message'] =  "Image Upload Successfully";
            $data['image'] = $imageUrl;
            return response()->json(["data" => $data]);
        }
    }

}
