<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $fillable = ['name','email','age','dob','address','password', 'profile_picture', 'role'];
    protected $table = 'user_profile';
}
