<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\Employeedata;
use Artisan;


class fetchData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:data {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'data fetch from api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        Employeedata::dispatch()->delay(now()->addSeconds(5));  
    }
}
