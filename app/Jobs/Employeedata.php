<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use App\Employee;
use DB;

class Employeedata implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $client = new Client([
        // Base URI is used with relative requests
        'base_uri' => 'http://dummy.restapiexample.com/api/v1/employees',
        // You can set any number of default request options.
        'timeout'  => 2.0,
        ]);
        $response=$client->request('GET');
        $body=$response->getBody();
        $data=json_decode($body);
        
        foreach ($data->data as $key => $value) {
                $employee = new Employee();
                $employee->name = $value->employee_name;
                $employee->salary = $value->employee_salary;
                $employee->age = $value->employee_age;
                $employee->profile_image = $value->profile_image;
                $employee->address = '';
                $employee->save();    
                // dd($employee);    

            echo '<pre>';
            print_r($value);
            echo '</pre>';
        }
    }
}
