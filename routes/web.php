<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use App\Employee;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// laravel ticket cru  example is here----------------
Route::get('/create/ticket','TicketController@create');
Route::post('/create/ticket','TicketController@store');
Route::get('/tickets', 'TicketController@index');
Route::get('/edit/ticket/{id}','TicketController@edit');
Route::patch('/edit/ticket/{id}','TicketController@update');
Route::delete('/delete/ticket/{id}','TicketController@destroy');


// Query Data using Laravel Eloquent Collection-------
Route::get('/user_eloquent_collection', function () {
    $users = \App\User::all();
    foreach ($users as $user) {
        echo '<pre>';
        echo $user->name;
        echo '</pre>';
    }
});

// Laravel chunk() Collection Method-------
Route::get('/user_chunk_collection', function () {
    $users = \App\User::all();
    $chunks = $users->chunk(2);
    $data = $chunks->toArray();
    echo '<pre>';
    print_r($data);
    echo '</pre>';
});

// Laravel custom Collection Method------- define in user model and app/CustomCollection 
Route::get('/user_custom_collection', function () {
    $users = \App\User::get();
    $users->gotAllUsers();
});


Route::get('/user_Api_data', function () {

    $client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'http://dummy.restapiexample.com/api/v1/employees',
    // You can set any number of default request options.
    'timeout'  => 2.0,
    ]);
    $response=$client->request('GET');
    $body=$response->getBody();
    $data=json_decode($body);
    // dd($data);
    foreach ($data->data as $key => $value) {
            $employee = new Employee();
            $employee->name = $value->employee_name;
            $employee->salary = $value->employee_salary;
            $employee->age = $value->employee_age;
            $employee->profile_image = $value->profile_image;
            $employee->address = '';
            $employee->save();    
            // dd($employee);    

        echo '<pre>';
        print_r($value);
        echo '</pre>';
    }
    dd('successfully added all records');
});

// Form valiation 
Route::get('form', 'FormController@create')->name('form.create');
Route::post('form', 'FormController@store')->name('form.store');